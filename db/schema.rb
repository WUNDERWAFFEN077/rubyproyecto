# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20171201200316) do

  create_table "alquileres", force: :cascade do |t|
    t.date    "fecha",                 null: false
    t.time    "hora_inicio",           null: false
    t.time    "hora_fin",              null: false
    t.integer "canchas_id",  limit: 4, null: false
  end

  add_index "alquileres", ["canchas_id"], name: "FK_alquileres_1", using: :btree

  create_table "alquileres_servicios", force: :cascade do |t|
    t.integer "alquileres_id", limit: 4, null: false
    t.integer "usuarios_id",   limit: 4, null: false
    t.integer "servicios_id",  limit: 4, null: false
  end

  add_index "alquileres_servicios", ["alquileres_id"], name: "FK_alquileres_canchas_1", using: :btree
  add_index "alquileres_servicios", ["servicios_id"], name: "FK_alquileres_canchas_3", using: :btree
  add_index "alquileres_servicios", ["usuarios_id"], name: "FK_alquileres_canchas_2", using: :btree

  create_table "canchas", force: :cascade do |t|
    t.string  "descripcion",     limit: 50,  null: false
    t.string  "promocion",       limit: 45
    t.string  "foto",            limit: 255
    t.integer "locales_id",      limit: 4,   null: false
    t.float   "importe_tarifa",  limit: 53
    t.integer "tipo_tarifas_id", limit: 4,   null: false
    t.integer "hora_de",         limit: 4,   null: false
    t.integer "hora_hasta",      limit: 4,   null: false
    t.boolean "dia_1"
    t.boolean "dia_2"
    t.boolean "dia_3"
    t.boolean "dia_4"
    t.boolean "dia_5"
    t.boolean "dia_6"
    t.boolean "dia_7"
  end

  add_index "canchas", ["locales_id"], name: "FK_canchas_1", using: :btree
  add_index "canchas", ["tipo_tarifas_id"], name: "FK_canchas_2", using: :btree

  create_table "comentarios", force: :cascade do |t|
    t.string  "descripcion",  limit: 50, null: false
    t.string  "calificacion", limit: 1
    t.integer "canchas_id",   limit: 4,  null: false
    t.integer "usuarios_id",  limit: 4,  null: false
  end

  add_index "comentarios", ["canchas_id"], name: "FK_comentarios_1", using: :btree
  add_index "comentarios", ["usuarios_id"], name: "FK_comentarios_2", using: :btree

  create_table "distritos", force: :cascade do |t|
    t.string "nombre", limit: 50
  end

  create_table "locales", force: :cascade do |t|
    t.string  "nombre",           limit: 50
    t.string  "direccion",        limit: 100
    t.string  "direccion_google", limit: 50
    t.string  "telefono",         limit: 10
    t.integer "usuarios_id",      limit: 4
    t.integer "distritos_id",     limit: 4
  end

  add_index "locales", ["distritos_id"], name: "FK_locales_2", using: :btree
  add_index "locales", ["usuarios_id"], name: "FK_locales_1", using: :btree

  create_table "personas", force: :cascade do |t|
    t.string  "nombre",             limit: 50
    t.string  "appaterno",          limit: 50
    t.string  "apmaterno",          limit: 50
    t.string  "sexo",               limit: 1
    t.string  "num_documento",      limit: 20
    t.string  "celular",            limit: 20
    t.date    "fec_nacimiento"
    t.string  "direccion",          limit: 100
    t.integer "tipo_documentos_id", limit: 4
    t.integer "usuarios_id",        limit: 4
  end

  add_index "personas", ["tipo_documentos_id"], name: "FK_personas_2", using: :btree
  add_index "personas", ["usuarios_id"], name: "FK_personas_1", using: :btree

  create_table "publicidades", force: :cascade do |t|
    t.string  "titulo",       limit: 45,    null: false
    t.text    "contenido",    limit: 65535, null: false
    t.date    "fecha_inicio",               null: false
    t.date    "fecha_fin",                  null: false
    t.float   "tarifa",       limit: 53,    null: false
    t.integer "ubicacion",    limit: 4,     null: false
    t.integer "usuarios_id",  limit: 4,     null: false
  end

  add_index "publicidades", ["usuarios_id"], name: "FK_publicidades_1", using: :btree

  create_table "servicios", force: :cascade do |t|
    t.string  "nombre",         limit: 50, null: false
    t.string  "tipo_servicio",  limit: 3,  null: false
    t.float   "importe_tarifa", limit: 53
    t.integer "locales_id",     limit: 4,  null: false
  end

  add_index "servicios", ["locales_id"], name: "FK_servicios_1", using: :btree

  create_table "tipo_documentos", force: :cascade do |t|
    t.string "nombre", limit: 50
  end

  create_table "tipo_personas", force: :cascade do |t|
    t.string "nombre", limit: 45, null: false
  end

  create_table "tipo_tarifas", force: :cascade do |t|
    t.string "descripcion", limit: 50, null: false
  end

  create_table "usuarios", force: :cascade do |t|
    t.string   "email",                  limit: 255, default: "", null: false
    t.string   "encrypted_password",     limit: 255, default: "", null: false
    t.string   "reset_password_token",   limit: 255
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.integer  "sign_in_count",          limit: 4,   default: 0,  null: false
    t.datetime "current_sign_in_at"
    t.datetime "last_sign_in_at"
    t.string   "current_sign_in_ip",     limit: 255
    t.string   "last_sign_in_ip",        limit: 255
    t.string   "name",                   limit: 255
    t.string   "permission_level",       limit: 255
    t.datetime "created_at",                                      null: false
    t.datetime "updated_at",                                      null: false
  end

  add_index "usuarios", ["email"], name: "index_usuarios_on_email", unique: true, using: :btree
  add_index "usuarios", ["reset_password_token"], name: "index_usuarios_on_reset_password_token", unique: true, using: :btree

  add_foreign_key "alquileres", "canchas", column: "canchas_id", name: "FK_alquileres_1"
  add_foreign_key "alquileres_servicios", "alquileres", column: "alquileres_id", name: "FK_alquileres_canchas_1"
  add_foreign_key "alquileres_servicios", "servicios", column: "servicios_id", name: "FK_alquileres_canchas_3"
  add_foreign_key "alquileres_servicios", "usuarios", column: "usuarios_id", name: "FK_alquileres_canchas_2"
  add_foreign_key "canchas", "locales", column: "locales_id", name: "FK_canchas_1"
  add_foreign_key "canchas", "tipo_tarifas", column: "tipo_tarifas_id", name: "FK_canchas_2"
  add_foreign_key "comentarios", "canchas", column: "canchas_id", name: "FK_comentarios_1"
  add_foreign_key "comentarios", "usuarios", column: "usuarios_id", name: "FK_comentarios_2"
  add_foreign_key "locales", "distritos", column: "distritos_id", name: "FK_locales_2"
  add_foreign_key "locales", "usuarios", column: "usuarios_id", name: "FK_locales_1"
  add_foreign_key "personas", "tipo_documentos", column: "tipo_documentos_id", name: "FK_personas_2"
  add_foreign_key "personas", "usuarios", column: "usuarios_id", name: "FK_personas_1"
  add_foreign_key "publicidades", "usuarios", column: "usuarios_id", name: "FK_publicidades_1"
  add_foreign_key "servicios", "locales", column: "locales_id", name: "FK_servicios_1"
end
