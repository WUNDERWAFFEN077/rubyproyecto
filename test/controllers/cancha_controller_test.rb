require 'test_helper'

class CanchaControllerTest < ActionController::TestCase
  test "should get index" do
    get :index
    assert_response :success
  end

  test "should get mostrar" do
    get :mostrar
    assert_response :success
  end

  test "should get crud" do
    get :crud
    assert_response :success
  end

end
