require File.join(Rails.root, "app", "models", "cancha.rb")
require File.join(Rails.root, "app", "models", "local.rb")
class CanchaController < ApplicationController
  before_action :authenticate_usuario!
  before_action :set_cancha, only: [:show, :edit, :update, :destroy]
  
  def index
    @curdate = Time.now.strftime("%Y-%m-%d")
    @publicidades = Publicidad.where(ubicacion:3)
    @publicidades = @publicidades.where("? BETWEEN fecha_inicio AND fecha_fin", @curdate)
    Rails.logger.debug(@publicidades)
    
    @canchas = Cancha.paginate(page: params[:page], per_page: 5)
    
    Rails.logger.debug(@canchas)
  end

  def edit
    
  end
  
  def update
    
     respond_to do |format|
      if @cancha.update(cancha_params)
        format.html { redirect_to '/cancha', notice: 'Actualizado correctamente.' }
      else
        format.html { render :new }
        format.json { render json: @local.errors, status: :unprocessable_entity }
      end
    end
    
  end

  def show
  end
  
  def destroy
    @cancha = Cancha.find_by(id: params[:id])
    @cancha.destroy
    respond_to do |format|
      format.html { redirect_to '/cancha', notice: 'Eliminado correctamente. ' }
      format.json { head :no_content }
    end
  end
  
  def new
    @cancha = Cancha.new
  end
  
  def create 
    @descripcion = params[:descripcion]   
    @locales_id = params[:locales_id]
    @importe_tarifa = params[:importe_tarifa]
    @dia_1 = params[:dia_1]
    @dia_2 = params[:dia_2]
    @dia_3 = params[:dia_3]
    @dia_4 = params[:dia_4]
    @dia_5 = params[:dia_5]
    @dia_6 = params[:dia_6]
    @dia_7 = params[:dia_7]
    @hora_de = params[:hora_de]
    @hora_hasta = params[:hora_hasta]
    @foto = params[:foto]
    
    @tipo_tarifas_id = params[:tipo_tarifas_id]
    
    
    
  
    
    @cancha =  Cancha.new(:descripcion => @descripcion, :locales_id => @locales_id, :tipo_tarifas_id => @tipo_tarifas_id,:importe_tarifa => @importe_tarifa,
    :dia_1 => @dia_1,:dia_2 => @dia_2,:dia_3 => @dia_3,:dia_4 => @dia_4,:dia_5 => @dia_5,:dia_6 => @dia_6,:dia_7 => @dia_7,
    :hora_de => @hora_de,:hora_hasta => @hora_hasta, :foto => @foto) 
    
    respond_to do |format|
      if @cancha.save
        format.html { redirect_to '/cancha', notice: 'Creado correctamente.' }
      else
        format.html { render 'new' }
        format.json { render json: @cancha.errors, status: :unprocessable_entity }
      end
    end
  end
  
  
  
  def set_cancha
      @cancha = Cancha.find(params[:id])
  end
    
  def cancha_params
      params.require(:cancha).permit(:descripcion, :locales_id, :tipo_tarifas_id,:importe_tarifa,:dia_1,:dia_2,:dia_3,:dia_4,:dia_5,:dia_6,:dia_7,:hora_de,:hora_hasta,:foto)
  end
  
  
 
end
