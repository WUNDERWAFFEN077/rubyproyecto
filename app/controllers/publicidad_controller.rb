class PublicidadController < ApplicationController
  before_action :authenticate_usuario!
  before_action :set_publicidad, only: [:show, :edit, :update, :destroy]
  
  def index
    @publicidades = Publicidad.paginate(page: params[:page], per_page: 5)
    
    Rails.logger.debug(@publicidades)
  end

  def edit
    
  end
  
  def update
    
     respond_to do |format|
      if @publicidad.update(publicidad_params)
        format.html { redirect_to '/publicidad', notice: 'Actualizado correctamente.' }
      else
        format.html { render :edit }
        format.json { render json: @publicidad.errors, status: :unprocessable_entity }
      end
    end
    
  end
  
  
  def new
    @publicidad = Publicidad.new
  end
  
  def create
    @titulo = params[:titulo]   
    @contenido = params[:contenido]
    @fecha_inicio = params[:fecha_inicio]
    @fecha_fin = params[:fecha_fin]
    @tarifa = params[:tarifa]
    @usuarios_id = current_usuario.id
    @ubicacion = params[:ubicacion]
    
    @publicidad =  Publicidad.new(:titulo => @titulo, :contenido => @contenido, :fecha_inicio => @fecha_inicio, 
    :fecha_fin => @fecha_fin, :tarifa => @tarifa, :usuarios_id => @usuarios_id, :ubicacion => @ubicacion) 
    
   
    
    respond_to do |format|
      if @publicidad.save
        format.html { redirect_to '/publicidad', notice: 'Creado correctamente.' }
      else
        format.html { render 'new' }
        format.json { render json: @publicidad.errors, status: :unprocessable_entity }
      end
    end
    
  end
  
  def destroy
    @publicidad = Publicidad.find_by(id: params[:id])
    @publicidad.destroy
    respond_to do |format|
      format.html { redirect_to '/publicidad', notice: 'Eliminado correctamente. ' }
      format.json { head :no_content }
    end
  end
  
  

  def set_publicidad
      @publicidad = Publicidad.find(params[:id])
  end
    
  def publicidad_params
      params.require(:publicidad).permit(:titulo, :contenido, :fecha_inicio, :fecha_fin, :tarifa,:ubicacion, :usuarios_id)
  end
  
end
