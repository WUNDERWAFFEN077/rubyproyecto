class PersonaController < ApplicationController
  before_action :authenticate_usuario!
  before_action :set_persona, only: [:show, :edit, :update, :destroy]
  
  def index
    @curdate = Time.now.strftime("%Y-%m-%d")
    @publicidades = Publicidad.where(ubicacion:3)
    @publicidades = @publicidades.where("? BETWEEN fecha_inicio AND fecha_fin", @curdate)
    Rails.logger.debug(@publicidades)
    
    #@persona = Persona.find_by(id: 1)
    @persona = Persona.find_by(usuarios_id: current_usuario.id)
  end
  
  def update
    
     respond_to do |format|
      if @persona.update(persona_params)
        format.html { redirect_to '/persona', notice: 'Actualizado correctamente.' }
      else
        format.html { render :index }
        format.json { render json: @persona.errors, status: :unprocessable_entity }
      end
    end
    
  end
  
  def set_persona
      @persona = Persona.find(params[:id])
  end
    
  def persona_params
      params.require(:persona).permit(:nombre, :appaterno, :apmaterno, :sexo, :tipo_documentos_id, :num_documento, :fec_nacimiento, :direccion)
  end
end
