class AlquilerController < ApplicationController
  before_action :authenticate_usuario!
  before_action :set_alquiler, only: [:show, :edit, :update, :destroy]
  
  def index
    @curdate = Time.now.strftime("%Y-%m-%d")
    @publicidades = Publicidad.where(ubicacion:3)
    @publicidades = @publicidades.where("? BETWEEN fecha_inicio AND fecha_fin", @curdate)
    Rails.logger.debug(@publicidades)
    
    @local = params[:local]
    @fecha_inicio = Date.parse(params[:fecha_inicio]).strftime("%Y-%m-%d") unless params[:fecha_inicio].blank?
    @fecha_fin = Date.parse(params[:fecha_fin]).strftime("%Y-%m-%d") unless params[:fecha_fin].blank?
    Rails.logger.debug(@fecha_inicio)
    #abort @fecha_inicio.inspect
    #@alquileres = Alquiler.where(:canchas_id => 1).paginate(page: params[:page], per_page: 3)
    #created_at: (Time.now.midnight - 1.day)..Time.now.midnight
    @alquileres = Alquiler.joins(:cancha).joins(:cancha => :local)
    @alquileres = @alquileres.where(:locales => {:id => @local}) unless @local.blank?
    @alquileres = @alquileres.where(:alquileres => {fecha: @fecha_inicio..@fecha_fin}) unless @fecha_inicio.blank? || @fecha_fin.blank? 
    @alquileres = @alquileres.paginate(page: params[:page], per_page: 4)
    respond_to do |format|
      format.html 
      format.js
    end
  end
  
  def alquilar
    @fecha = params[:fecha]
    @dia = params[:dia]
    @hora = params[:hora]
    
    @servicios = Servicio.all
    @alquiler =  Alquiler.new
    
    @cancha = Cancha.find_by(id: params[:id])
  end
  
  def confirmar
    @alquiler = Alquiler.find_by(id: params[:id])
    #Crear estado para actualizar a confirmado
    @estado = 'RESERVADO'
    @alquiler.update(:estado => @estado)
    
    
    respond_to do |format|
      format.html { redirect_to :back, notice: 'La cancha ha sido reservada.' }
      format.json { head :no_content }
    end
    
  end
  
  
  def create
    @fecha = params[:fecha]
    @hora_inicio = params[:hora]
    @hora_fin = params[:hora]
    @canchas_id = params[:canchas_id]
    @servicios = params[:servicio]
    @usuarios_id = current_usuario.id
    @estado = 'PENDIENTE'
    #abort @servicio.inspect
    #abort @servicio.inspect
    
    
    
    #abort @servicio.size.inspect
 
    
    respond_to do |format|
      begin
        @alquiler =  Alquiler.new(:fecha => @fecha,:hora_inicio => @hora_inicio,:hora_fin => @hora_fin,:canchas_id => @canchas_id,:estado => @estado)
        ActiveRecord::Base.transaction do
          @alquiler.save!
          
          #Obtener el ultimo id insertado
          @alquileres_id = @alquiler.id
          
          @servicios.each do |servicio|
            #@alquiler_servicio = AlquilerServicio.new(:alquileres_id => @alquileres_id,:servicios_id => 10)
            @alquiler_servicio = AlquilerServicio.new(:alquileres_id => @alquileres_id, :usuarios_id => @usuarios_id,:servicios_id => servicio)
            @alquiler_servicio.save!
          end
        end
        #handle success here
        format.html { redirect_to :back, notice: 'Creado correctamente.' }
      rescue Exception => e
          #raise ActiveRecord::Rollback #force a rollback
          #handle failure here
          logger.info e.message
          #format.html { redirect_to :back, notice: 'No se pudo realizar la transacción.' }
          format.html { redirect_to :back, notice: e.message }
      end
    end
    
  end
  
  
  def set_alquiler
      @alquiler = Alquiler.find(params[:id])
  end
    
  def alquiler_params
      params.require(:alquiler).permit(:fecha, :hora_inicio, :hora_fin,:canchas_id,:estado)
  end
end
