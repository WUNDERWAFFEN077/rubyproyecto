class ComentarioController < ApplicationController
    def create 
        @descripcion = params[:descripcion]
        @calificacion = params[:calificacion]
        @usuarios_id = current_usuario.id
        @canchas_id = params[:canchas_id]
        @locales_id = params[:locales_id]
        
        @local =  Comentario.new(:descripcion => @descripcion,:calificacion => @calificacion, :usuarios_id => @usuarios_id, :canchas_id => @canchas_id)
        
        
        
        respond_to do |format|
            @local.save
            format.html { redirect_to '/home/'+@locales_id, notice: 'Comentario agregado correctamente.' }
        end
           
    
    end
end
