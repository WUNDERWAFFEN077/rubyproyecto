require File.join(Rails.root, "app", "models", "local.rb")
require File.join(Rails.root, "app", "models", "distrito.rb")

class LocalController < ApplicationController
  before_action :authenticate_usuario!
  before_action :set_local, only: [:show, :edit, :update, :destroy]
  
  def index
    @curdate = Time.now.strftime("%Y-%m-%d")
    @publicidades = Publicidad.where(ubicacion:3)
    @publicidades = @publicidades.where("? BETWEEN fecha_inicio AND fecha_fin", @curdate)
    Rails.logger.debug(@publicidades)
    
    @locales = Local.paginate(page: params[:page], per_page: 5)
    
    #@locales = Local.where(usuarios_id:current_usuario.id)
    
    Rails.logger.debug(@locales)
  end

  def edit
    
  end
  
  def update
    
     respond_to do |format|
      if @local.update(local_params)
        format.html { redirect_to '/local', notice: 'Actualizado correctamente.' }
      else
        format.html { render 'crud' }
        format.json { render json: @local.errors, status: :unprocessable_entity }
      end
    end
    
  end

  def new
    @local = Local.new
    @distritos = Distrito.all
    
    render 'crud'
  end
  
  def create 
    @nombre = params[:nombre]   
    @direccion = params[:direccion]
    @direccion_google = params[:direccion_google]
    @telefono = params[:telefono]
    @usuarios_id = current_usuario.id
    @distritos_id = params[:distritos_id]
    
    @local =  Local.new(:nombre => @nombre, :direccion => @direccion, :direccion_google=>@direccion_google, 
    :telefono=>@telefono, :usuarios_id=>@usuarios_id,:distritos_id=>@distritos_id)
    
    
    
     respond_to do |format|
      if @local.save
        format.html { redirect_to '/local', notice: 'Creado correctamente.' }
      else
        format.html { render 'crud' }
        format.json { render json: @local.errors, status: :unprocessable_entity }
      end
    end
    
  
       

  end
  
  def show
  end
  
  def destroy
    @local = Local.find_by(id: params[:id])
    @local.destroy
    #redirect_to '/local'
    respond_to do |format|
      format.html { redirect_to '/local', notice: 'Eliminado correctamente. ' }
      format.json { head :no_content }
    end
  end
  
  def set_local
      @local = Local.find(params[:id])
  end
    
  def local_params
      params.require(:local).permit(:nombre, :direccion, :distritos_id, :direccion_google, :telefono)
  end
end
