require File.join(Rails.root, "app", "models", "persona.rb")
require File.join(Rails.root, "app", "models", "tipo_documento.rb")

class RegistrationsController < Devise::RegistrationsController
    private def sign_up_params
        params.require(:usuario).permit( :email, :password,:password_confirmation,:permission_level,
        persona_attributes:[:nombre,:appaterno,:apmaterno,:sexo,:num_documento,:fec_nacimiento,:direccion,:tipo_documentos_id])
    end
    
    
   def new
        @tiposdocumento = TipoDocumento.all
       
        build_resource({})
        resource.build_persona
   end

  
   
   
  
end
