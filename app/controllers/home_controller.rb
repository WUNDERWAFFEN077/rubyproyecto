class HomeController < ApplicationController
  def index
    @publicidades = Publicidad.where(ubicacion:1)
    @locales = Local.all
  end
  
  def show
    @local = Local.find_by(id: params[:id])
    
    #@canchas = Cancha.find_by(locales_id:params[:id])
    @canchas = Cancha.where(locales_id:params[:id])
  end
  
  def reservar
    @sql = "SELECT DATE_FORMAT(mydate,'%H:%i') AS hora
    FROM(
    SELECT CONCAT(CURDATE(),' 00:00:00') + INTERVAL (d0*10+d1) HOUR  AS mydate
    FROM (
    SELECT 0 AS d0 UNION SELECT 1 UNION SELECT 2
    ) AS t1
    CROSS JOIN (
    SELECT 0 AS d1 UNION SELECT 1 UNION SELECT 2 UNION SELECT 3 UNION SELECT 4 UNION 
    SELECT 5 UNION SELECT 6 UNION SELECT 7 UNION SELECT 8 UNION SELECT 9 ) AS t2
    WHERE (d0*10+d1) < 24
    ORDER BY d0,d1
    )AS dt
    WHERE dt.mydate BETWEEN CONCAT(CURDATE(),' 06:00:00') AND CONCAT(CURDATE(),' 23:00:00');"
    @horas = ActiveRecord::Base.connection.execute(@sql)
    
    @sql = "SELECT selected_date AS  fecha,
    CASE DAYNAME(selected_date)
    WHEN 'Monday' THEN 'Lunes'
    WHEN 'Tuesday' THEN 'Martes'
    WHEN 'Wednesday' THEN 'Miercoles'
    WHEN 'Thursday' THEN 'Jueves'
    WHEN 'Friday' THEN 'Viernes'
    WHEN 'Saturday' THEN 'Sabado'
    WHEN 'Sunday' THEN 'Domingo'
    ELSE ''
    END
    AS nombre
     
    FROM 
    (SELECT ADDDATE('1970-01-01',t4.i*10000 + t3.i*1000 + t2.i*100 + t1.i*10 + t0.i) selected_date FROM
     (SELECT 0 i UNION SELECT 1 UNION SELECT 2 UNION SELECT 3 UNION SELECT 4 UNION SELECT 5 UNION SELECT 6 UNION SELECT 7 UNION SELECT 8 UNION SELECT 9) t0,
     (SELECT 0 i UNION SELECT 1 UNION SELECT 2 UNION SELECT 3 UNION SELECT 4 UNION SELECT 5 UNION SELECT 6 UNION SELECT 7 UNION SELECT 8 UNION SELECT 9) t1,
     (SELECT 0 i UNION SELECT 1 UNION SELECT 2 UNION SELECT 3 UNION SELECT 4 UNION SELECT 5 UNION SELECT 6 UNION SELECT 7 UNION SELECT 8 UNION SELECT 9) t2,
     (SELECT 0 i UNION SELECT 1 UNION SELECT 2 UNION SELECT 3 UNION SELECT 4 UNION SELECT 5 UNION SELECT 6 UNION SELECT 7 UNION SELECT 8 UNION SELECT 9) t3,
     (SELECT 0 i UNION SELECT 1 UNION SELECT 2 UNION SELECT 3 UNION SELECT 4 UNION SELECT 5 UNION SELECT 6 UNION SELECT 7 UNION SELECT 8 UNION SELECT 9) t4) v
    WHERE YEARWEEK(`selected_date`, 1) = YEARWEEK(CURDATE(), 1)
    ORDER BY selected_date;"
    @dias = ActiveRecord::Base.connection.execute(@sql)
    
    @cancha = Cancha.find_by(id: params[:id])
    
    @servicios = Servicio.joins(:local).joins(:local => :canchas)
    @servicios = @servicios.where(:canchas => {:id =>params[:id]})
  end
  
  def alquilar
    @fecha = params[:fecha]
    @dia = params[:dia]
    @hora = params[:hora]
   
    @alquiler =  Alquiler.new
    
    @cancha = Cancha.find_by(id: params[:id])
    #@servicios = Servicio.all
    @servicios = Servicio.joins(:local).joins(:local => :canchas)
    @servicios = @servicios.where(:canchas => {:id =>params[:id]})
  end
  
  def create
    
    
  end
  
end
