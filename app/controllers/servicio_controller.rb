class ServicioController < ApplicationController
  before_action :authenticate_usuario!
  before_action :set_servicio, only: [:show, :edit, :update, :destroy]
  
  def index
    @curdate = Time.now.strftime("%Y-%m-%d")
    @publicidades = Publicidad.where(ubicacion:3)
    @publicidades = @publicidades.where("? BETWEEN fecha_inicio AND fecha_fin", @curdate)
    Rails.logger.debug(@publicidades)
    @servicios = Servicio.paginate(page: params[:page], per_page: 5)
  end

  def new
     @servicio = Servicio.new
  end
  
  def edit
    
  end
  
  def update
    
     respond_to do |format|
      if @servicio.update(servicio_params)
        format.html { redirect_to '/servicio', notice: 'Actualizado correctamente.' }
      else
        format.html { render :new }
        format.json { render json: @servicio.errors, status: :unprocessable_entity }
      end
    end
    
  end
  
  def destroy
    @servicio = Servicio.find_by(id: params[:id])
    @servicio.destroy
    respond_to do |format|
      format.html { redirect_to '/servicio', notice: 'Eliminado correctamente. ' }
      format.json { head :no_content }
    end
  end
  
  
  def create 
    @nombre = params[:nombre]   
    @locales_id = params[:locales_id]
    @importe_tarifa = params[:importe_tarifa]
    @tipo_servicio = params[:tipo_servicio]
  
    
    @servicio = Servicio.new(:nombre => @nombre, :locales_id => @locales_id,:importe_tarifa => @importe_tarifa,:tipo_servicio => @tipo_servicio) 
    
    respond_to do |format|
      if @servicio.save
        format.html { redirect_to '/servicio', notice: 'Creado correctamente.' }
      else
        format.html { render 'new' }
        format.json { render json: @servicio.errors, status: :unprocessable_entity }
      end
    end
  end
  
  def show
  end
  
  def set_servicio
      @servicio = Servicio.find(params[:id])
  end
    
  def servicio_params
      params.require(:servicio).permit(:nombre,:tipo_servicio,:importe_tarifa,:locales_id)
  end
end
