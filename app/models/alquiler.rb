 class Alquiler < ActiveRecord::Base
     self.table_name = "alquileres"
     #belongs_to :cancha
     belongs_to :cancha, :class_name => "Cancha", :foreign_key => "canchas_id"
     has_many :alquiler_servicios
     
      
     validates :fecha, presence: true
     validates :hora_inicio, presence: true
     validates :hora_fin, presence: true
     validates :canchas_id, presence: true
      
 end