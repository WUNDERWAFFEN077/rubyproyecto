 class Local < ActiveRecord::Base
      self.table_name = "locales"
      
      
      belongs_to :usuario
      #belongs_to :distrito
      belongs_to :distrito, :class_name => "Distrito", :foreign_key => "distritos_id"
      has_many :canchas, :class_name => "Cancha", :foreign_key => "locales_id"
      has_many :servicios
      
      
      validates :nombre, presence: true
      validates :direccion, presence: true
      validates :distritos_id, presence: true
      
      
      accepts_nested_attributes_for :distrito
 end