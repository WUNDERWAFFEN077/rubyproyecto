 class Cancha < ActiveRecord::Base
     mount_uploader :foto, FotoUploader
     
     belongs_to :local, :class_name => "Local", :foreign_key => "locales_id"
     belongs_to :tipo_tarifa, :class_name => "TipoTarifa", :foreign_key => "tipo_tarifas_id"
     has_many :alquileres, :class_name => "Alquiler", :foreign_key => "canchas_id"
     has_many :comentarios, :class_name => "Comentario", :foreign_key => "canchas_id"
     
     
    
     
     
     
      
     validates :descripcion, presence: true
     validates :locales_id, presence: true
     validates :tipo_tarifas_id, presence: true
     validates :hora_de, presence: true
     validates :hora_hasta, presence: true
     validates :importe_tarifa, presence: true
      
 end