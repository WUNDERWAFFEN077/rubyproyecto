 class Publicidad < ActiveRecord::Base
     self.table_name = "publicidades"
  
     belongs_to :usuario
     
     validates :titulo, presence: true
     validates :contenido, presence: true
     validates :fecha_inicio, presence: true
     validates :fecha_fin, presence: true
     validates :tarifa, presence: true
     validates :ubicacion, presence: true
 end