 class Servicio < ActiveRecord::Base
     
     belongs_to :local, :class_name => "Local", :foreign_key => "locales_id"
     
     validates :nombre, presence: true
     validates :tipo_servicio, presence: true
     validates :importe_tarifa, presence: true
     validates :locales_id, presence: true
 end