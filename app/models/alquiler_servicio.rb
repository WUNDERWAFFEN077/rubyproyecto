 class AlquilerServicio < ActiveRecord::Base
     self.table_name = "alquileres_servicios"
     belongs_to :alquiler
     
      
     validates :alquileres_id, presence: true
     validates :usuarios_id, presence: true
     validates :servicios_id, presence: true
      
 end