class Usuario < ActiveRecord::Base
  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable and :omniauthable
  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :trackable, :validatable
  
  has_one :persona, :class_name => "Persona", :foreign_key => "usuarios_id"
  has_many  :locales
  has_many  :publicidades
  has_many :comentarios, :class_name => "Comentario", :foreign_key => "usuarios_id"
  
  
  accepts_nested_attributes_for :persona
end
