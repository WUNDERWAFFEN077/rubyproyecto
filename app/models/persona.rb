 class Persona < ActiveRecord::Base
   belongs_to  :usuario, :class_name => "Usuario", :foreign_key => "usuarios_id"
   #has_many :usuario
   belongs_to :tipo_documento
     
   validates :tipo_documentos_id, presence: true
 end