Rails.application.routes.draw do
  get 'persona/index'

  devise_for :usuarios, controllers: {registrations: 'registrations'}
  get 'welcome/index'

  root to: 'home#index'

  
  #get 'local/index'

  #get 'local/mostrar'

  #get 'local/crud'
  
  #post 'local/create'
  get 'home/buscar_canchas' => 'home#buscar_canchas', as: :buscar_canchas
  
  post 'comentario/create'
  
  resources :local
  resources :cancha
  resources :servicio
  resources :publicidad
  resources :home
  resources :persona
  resources :alquiler
  
  
  
  post 'alquiler/buscar'=> 'alquiler#index', as: :buscar
  put 'alquiler/confirmar/:id(.:format)'=> 'alquiler#confirmar', as: :confirmar
  
  get 'home/:id/reservar'=> 'home#reservar', as: :reservar
  get 'home/:id/alquilar'=> 'home#alquilar', as: :alquilar
 

  # The priority is based upon order of creation: first created -> highest priority.
  # See how all your routes lay out with "rake routes".

  # You can have the root of your site routed with "root"
  # root 'welcome#index'

  # Example of regular route:
  #   get 'products/:id' => 'catalog#view'

  # Example of named route that can be invoked with purchase_url(id: product.id)
  #   get 'products/:id/purchase' => 'catalog#purchase', as: :purchase

  # Example resource route (maps HTTP verbs to controller actions automatically):
  #   resources :products

  # Example resource route with options:
  #   resources :products do
  #     member do
  #       get 'short'
  #       post 'toggle'
  #     end
  #
  #     collection do
  #       get 'sold'
  #     end
  #   end

  # Example resource route with sub-resources:
  #   resources :products do
  #     resources :comments, :sales
  #     resource :seller
  #   end

  # Example resource route with more complex sub-resources:
  #   resources :products do
  #     resources :comments
  #     resources :sales do
  #       get 'recent', on: :collection
  #     end
  #   end

  # Example resource route with concerns:
  #   concern :toggleable do
  #     post 'toggle'
  #   end
  #   resources :posts, concerns: :toggleable
  #   resources :photos, concerns: :toggleable

  # Example resource route within a namespace:
  #   namespace :admin do
  #     # Directs /admin/products/* to Admin::ProductsController
  #     # (app/controllers/admin/products_controller.rb)
  #     resources :products
  #   end
end
